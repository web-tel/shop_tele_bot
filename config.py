from pydantic import BaseSettings
import os
import ast


class FlaskConfig(BaseSettings):
    URI: str
    DEFAULT_MONGODB_SETTINGS = {'db': 'something', 'host': 'localhost', 'alias': 'default'}
    MONGODB_SETTINGS = DEFAULT_MONGODB_SETTINGS
    DEBUG: bool = True
    SECRET_KEY: str
    SECURITY_PASSWORD_SALT: str
    ENVIRONMENT: str = "development"
    FLASK_DEBUG = 1

    class Config:
        # env_prefix = 'FLASK_'
        env_file = '.env'


class TelegramConfig(BaseSettings):
    BOT_TOKEN: str
    WEBHOOK_HOST: str
    WEBHOOK_PORT: int
    WEBHOOK_LISTEN: str
    WEBHOOK_SSL_CERT: str
    WEBHOOK_SSL_PRIV: str

    class Config:
        env_file = '.env'

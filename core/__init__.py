class AppContext(object):
    __instance = None

    def __new__(cls):
        if AppContext.__instance is None:
            AppContext.__instance = object.__new__(cls)
        return AppContext.__instance


core_context = AppContext()

import telebot

def in_line_keyboard_maker():
    bot_keyboard = telebot.types.InlineKeyboardMarkup()

    for item in [1,2,3]:
        bot_keyboard.add(telebot.types.InlineKeyboardButton(text=item,
                                                            callback_data=item))

    return bot_keyboard

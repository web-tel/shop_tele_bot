import telebot

class TeleBot:
    def __init__(self, telegram_config):
        self.bot = telebot.TeleBot(telegram_config.BOT_TOKEN)

    def send_text_message(self, chat_id: str, message: str, reply_markup):
        self.bot.send_message(self.chat_id, message, reply_markup=reply_markup)


def tele_bot_maker(telegram_config):
    return TeleBot(telegram_config)

from .models import Category


class CategoryRepository:
    def list(self):
        return Category.objects(active=True)

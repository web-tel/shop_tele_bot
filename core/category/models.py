from mongoengine import Document, StringField, BooleanField


class Category(Document):
    title = StringField()
    active = BooleanField(default=True)

    def __str__(self):
        return self.title

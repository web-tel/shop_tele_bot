from flask_mongoengine import BaseQuerySet, Document
from bson import DBRef, ObjectId
import ast


class NoDereferenceBaseQuerySet(BaseQuerySet):
    _auto_dereference = False

    def get_field_list_old(self, referrence_field='_id'):
        func = """
            function(path) {
                var ids = db[collection].find(query, {%s: 1}).map(function(item){ return item.%s; })
                return ids
            }
        """ % (referrence_field, referrence_field)
        ids = self.exec_js(func)
        ids = [ObjectId(i) for i in ids]
        return ids

    def get_field_list(self, field='_id'):
        if not self:
            return []
        map_function = """ function(){
            emit(this[field], 'none')
        }"""
        reduce_function = """ function(item, sec){
            return item;
        }"""
        return [i.key for i in
                self.map_reduce(map_f=map_function, reduce_f=reduce_function, scope={'field': field}, output='inline')]


class BaseDocument(Document):
    meta = {'abstract': True,
            'strict': False,
            'queryset_class': NoDereferenceBaseQuerySet,
            }

    def to_dict(self):
        result: dict = ast.literal_eval(self.to_json())
        result.update({'id': str(self.id)})
        return result

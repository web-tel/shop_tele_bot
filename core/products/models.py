from mongoengine import Document, StringField, BooleanField, DecimalField, ImageField


class Product(Document):
    title = StringField()
    description = StringField()
    price = DecimalField()
    image = ImageField(collection_name='images')
    category_id = StringField()
    active = BooleanField(default=True)

    def __str__(self):
        return self.title

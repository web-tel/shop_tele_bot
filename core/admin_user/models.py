from mongoengine import DateTimeField, StringField, BooleanField, IntField, Document, ListField, ReferenceField


class AdminRole(Document):
    meta = {
        "allow_inheritance": True
    }
    name = StringField(max_length=80, unique=True)
    description = StringField(max_length=255)
    permissions = StringField(max_length=255)


class AdminUser(Document):
    meta = {
        "allow_inheritance": True
    }
    login = StringField(max_length=40)
    password = StringField(max_length=256)
    active = BooleanField(default=True)
    fs_uniquifier = StringField(max_length=255)
    roles = ListField(ReferenceField(AdminRole), default=[], max_length=1)
    login_count = IntField(default=0)
    current_login_at = DateTimeField()
    current_login_ip = StringField(max_length=40)
    last_login_at = DateTimeField()
    last_login_ip = StringField(max_length=40)


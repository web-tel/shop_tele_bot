from .models import AdminUser

class AdminUserRepository:
    def get_admin_user_by_login(self, login):
        return AdminUser.objects.clear_cls_query().get(login=login)

    def get_all(self):
        return AdminUser.objects.clear_cls_query()
import os
import traceback
from logging import getLogger

from flask import Flask as _Flask, request


class Flask(_Flask):
    def log_exception(self, exc_info):
        super(Flask, self).log_exception(exc_info=exc_info)
        try:
            raise
        except Exception:
            getLogger().exception('FLASK Exception on {path} with traceback: {traceback}'.format(
                path=request.path,
                traceback=traceback.format_exc())
            )


def create_backend_app(config_obj):
    from flask import Flask
    flask_app = Flask(__name__, root_path=os.getcwd())
    flask_app.config.from_object(config_obj)
    print(flask_app.config)
    return flask_app

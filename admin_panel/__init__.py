from flask_admin.consts import ICON_TYPE_FONT_AWESOME
from flask_admin.menu import MenuLink

from . import (admin_user, category, products)


def add_admin_views(admin):
    admin_user.admin_init(admin)
    admin.add_link(
        MenuLink("Logout", url='/logout', icon_type=ICON_TYPE_FONT_AWESOME, icon_value="fa-sign-out fa-lg red"))
    category.admin_init(admin)
    products.admin_init(admin)

from flask_admin import expose, AdminIndexView
from flask_admin.contrib.mongoengine import ModelView
from flask_security import login_required



class IndexView(AdminIndexView):
    @expose()
    # @auth_required
    #@login_required
    def index(self):
        return self.render(self._template)

    def is_accessible(self):
        return True


class BaseModelView(ModelView):
    def get_query(self):
        query = super(BaseModelView, self).get_query()
        return query


from flask_mongoengine import MongoEngine
from flask_mongoengine import MongoEngineSessionInterface
from flask_security import MongoEngineUserDatastore, Security

from .forms import AdminLoginForm
from .security_models import AdminRole, AdminUser

db = MongoEngine()


def init_admin_security(app):
    user_datastore = MongoEngineUserDatastore(db, user_model=AdminUser, role_model=AdminRole)
    app.session_interface = MongoEngineSessionInterface(db, collection='admin_session')

    # Create a user to test with
    # @app.before_first_request
    # def create_user():
    #     user_datastore.create_user(login="admin", password="admin")
    Security(app, user_datastore, login_form=AdminLoginForm)

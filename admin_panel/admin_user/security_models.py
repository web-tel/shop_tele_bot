from flask_security import UserMixin, RoleMixin

from core.admin_user import models


class AdminRole(models.AdminRole, RoleMixin):
    pass


class AdminUser(models.AdminUser, UserMixin):
    pass

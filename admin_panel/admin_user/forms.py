from flask import request, current_app
from flask_security.forms import NextFormMixin, get_form_field_label, Form, password_required, \
    config_value, Markup, url_for_security, get_message, hash_password, _datastore, requires_confirmation, Required
from wtforms import StringField, PasswordField, BooleanField, SubmitField

login_required = Required(message="LOGIN_NOT_PROVIDED")


class AdminLoginForm(Form, NextFormMixin):
    email = StringField()
    login = StringField(get_form_field_label("login"),
                        filters=[lambda x: x.strip() if x else None], validators=[login_required])

    password = PasswordField(
        get_form_field_label("password"), validators=[password_required]
    )
    remember = BooleanField(get_form_field_label("remember_me"))
    submit = SubmitField(get_form_field_label("login"))

    def __init__(self, *args, **kwargs):
        super(AdminLoginForm, self).__init__(*args, **kwargs)
        if not self.next.data:
            self.next.data = request.args.get("next", "")
        self.remember.default = config_value("DEFAULT_REMEMBER_ME")
        if (
                current_app.extensions["security"].recoverable
                and not self.password.description
        ):
            html = Markup(
                u'<a href="{url}">{message}</a>'.format(
                    url=url_for_security("forgot_password"),
                    message=get_message("FORGOT_PASSWORD")[0],
                )
            )
            self.password.description = html

    def validate(self):
        if not super(AdminLoginForm, self).validate():
            return False

        self.user = _datastore.get_user(self.login.data)

        if self.user is None:
            self.login.errors.append(get_message("USER_DOES_NOT_EXIST")[0])
            # Reduce timing variation between existing and non-existing users
            hash_password(self.password.data)
            return False
        if not self.user.password:
            self.password.errors.append(get_message("PASSWORD_NOT_SET")[0])
            # Reduce timing variation between existing and non-existing users
            hash_password(self.password.data)
            return False
        if not self.user.verify_and_update_password(self.password.data):
            self.password.errors.append(get_message("INVALID_PASSWORD")[0])
            return False
        if requires_confirmation(self.user):
            self.login.errors.append(get_message("CONFIRMATION_REQUIRED")[0])
            return False
        if not self.user.is_active:
            self.login.errors.append(get_message("DISABLED_ACCOUNT")[0])
            return False
        return True

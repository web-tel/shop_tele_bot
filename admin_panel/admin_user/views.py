from admin_panel.admin_app.views import BaseModelView

from core.admin_user.models import AdminUser, AdminRole


class AdminUserView(BaseModelView):
    can_create = True
    can_delete = True
    column_editable_list = ["active"]
    column_exclude_list = ["_cls", "fs_uniquifier"]

    def __init__(self, name=None, category=None, endpoint=None, url=None):
        super(AdminUserView, self).__init__(AdminUser, name, category, endpoint, url)


class AdminRoleView(BaseModelView):
    can_create = True

    def __init__(self, name=None, category=None, endpoint=None, url=None):
        super(AdminRoleView, self).__init__(AdminRole, name, category, endpoint, url)

from .views import AdminUserView, AdminRoleView


def admin_init(admin, category=None):
    admin.add_view(AdminRoleView(name='AdminRole', category=category))
    admin.add_view(AdminUserView(name='AdminUser', category=category))

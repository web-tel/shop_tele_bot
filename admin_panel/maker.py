import os
import traceback
from logging import getLogger
from typing import List

from flask import Flask as _Flask, request
from flask_admin import Admin
from flask_admin.consts import ICON_TYPE_FONT_AWESOME

from admin_panel.admin_app.views import IndexView


class Flask(_Flask):
    def log_exception(self, exc_info):
        super(Flask, self).log_exception(exc_info=exc_info)
        try:
            raise
        except Exception:
            getLogger().exception('FLASK Exception on {path} with traceback: {traceback}'.format(
                path=request.path,
                traceback=traceback.format_exc())
            )


def create_admin_panel_app(config_obj: object = object(), name="") -> List[object]:
    flask_app = Flask(__name__, root_path=os.getcwd())
    flask_app.config.from_object(config_obj)


    adminka = Admin(flask_app, name=name, url="/", index_view=IndexView(url='/', menu_icon_type=ICON_TYPE_FONT_AWESOME,
                                                                        menu_icon_value="fa-home fa-lg",
                                                                        name="Home"))
    return flask_app, adminka

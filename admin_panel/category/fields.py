from gettext import gettext as _

from flask import current_app
from flask_mongoengine.wtf.fields import ModelSelectField
from mongoengine.queryset import DoesNotExist
from wtforms.validators import ValidationError




class CategorySelectField(ModelSelectField):
    def __init__(self, label='', validators=None, allow_blank=True, **kwargs):
        queryset = kwargs.pop('queryset', current_app.core_context.category_repository.list())
        super(ModelSelectField, self).__init__(label, validators, queryset=queryset, **kwargs)

    def iter_choices(self):
        if self.allow_blank:
            yield (u'__None', self.blank_text, self.data is None)

        if self.queryset is None:
            return

        self.queryset.rewind()
        for obj in self.queryset:
            label = self.label_attr and getattr(obj, self.label_attr) or obj
            if isinstance(self.data, list):
                selected = obj in self.data
            else:
                selected = self._is_selected(obj)
            yield (obj.id, label, selected)

    def process_formdata(self, valuelist):
        if valuelist:
            if valuelist[0] == '__None':
                self.data = None
            else:
                if self.queryset is None:
                    self.data = None
                    return

                try:
                    obj = self.queryset.get(pk=valuelist[0])
                    self.data = str(obj.id)
                except DoesNotExist:
                    self.data = None

    def pre_validate(self, form):
        if not self.allow_blank or self.data is not None:
            if not self.data:
                raise ValidationError(_(u'Not a valid choice'))

    def _is_selected(self, item):
        return str(item.id) == self.data

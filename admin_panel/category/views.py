from admin_panel.admin_app.views import BaseModelView
from core.category.models import Category


class CategoryView(BaseModelView):
    can_create = True
    can_delete = True
    column_list = ["title", "active",]

    def __init__(self, name=None, category=None, endpoint=None, url=None):
        super(CategoryView, self).__init__(Category, name, category, endpoint, url)

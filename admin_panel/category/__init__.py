def admin_init(admin, category=None):
    from .views import CategoryView
    admin.add_view(CategoryView(name='Категории', category=category))

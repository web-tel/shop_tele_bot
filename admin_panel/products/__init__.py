def admin_init(admin, category=None):
    from .views import ProductView
    admin.add_view(ProductView(name='Товары', category=category))

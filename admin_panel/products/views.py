from admin_panel.admin_app.views import BaseModelView
from core.products.models import Product
from admin_panel.category.fields import CategorySelectField


class ProductView(BaseModelView):
    can_create = True
    can_delete = True
    column_list = ["title", "active", "description", "price", "image", "category_id"]

    form_overrides = dict(
        category_id=CategorySelectField
    )

    def __init__(self, name=None, category=None, endpoint=None, url=None):
        super(ProductView, self).__init__(Product, name, category, endpoint, url)

from flask_babelex import Babel
from flask_mongoengine import MongoEngine

from admin_panel import add_admin_views
from admin_panel.maker import create_admin_panel_app
from admin_panel.admin_user.helpers import init_admin_security
from config import AdminkaConfig

app, admin_panel = create_admin_panel_app(config_obj=AdminkaConfig)
babel = Babel(app)

MongoEngine().init_app(app)
init_admin_security(app)
add_admin_views(admin_panel)

@app.before_first_request
def add_repos_and_services():
    from core import core_context
    from core.category.repository import CategoryRepository
    app.core_context = core_context
    #from core.admin_user import AdminUserRepository
    #app.core_context.admin_user_repository = AdminUserRepository()
    app.core_context.category_repository = CategoryRepository()


#@app.before_request
#def some_test():
#     from flask import current_app
#     obj = current_app.core_context.admin_user_repository.get_admin_user_by_login("admin")
#     print(obj.login)

@babel.localeselector
def get_locale():
    return 'ru'


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5001)

from flask_mongoengine import MongoEngine
import time
from flask import request, abort

from config import FlaskConfig, TelegramConfig
from backend.maker import create_backend_app
from flask_cors import CORS
import telebot

from core.bot.maker import tele_bot_maker

app = create_backend_app(config_obj=FlaskConfig())
CORS(app)
MongoEngine().init_app(app)
telegram_config = TelegramConfig()

bot_obj = tele_bot_maker(telegram_config)


# bot = telebot.TeleBot(telegram_config.BOT_TOKEN)


@app.before_first_request
def add_repos_and_services():
    from core import core_context
    from core.category.repository import CategoryRepository
    app.core_context = core_context
    app.core_context.category_repository = CategoryRepository()
    app.core_context.bot_obj = bot_obj


@app.route('/')
def index():
    return 'HELLO'


@app.route('/{}'.format(telegram_config.BOT_TOKEN), methods=['POST'])
def webhook():
    if request.headers.get('content-type') == 'application/json':
        json_string = request.get_data().decode('utf-8')
        update = telebot.types.Update.de_json(json_string)
        bot_obj.bot.process_new_updates([update])
        return ''
    else:
        abort(403)


# Handle '/start' and '/help'
@bot_obj.bot.message_handler(commands=['help', 'start'])
def send_welcome(message):
    print(app.core_context.category_repository.list())
    bot_obj.bot.reply_to(message,
                         ("Hi ввв there, I am EchoBot.\n"
                          "I am here to echo your kind words back to you."))


# Handle all other messages
@bot_obj.bot.message_handler(func=lambda message: True, content_types=['text'])
def echo_message(message):
    bot_obj.bot.reply_to(message, message.text)


# Remove webhook, it fails sometimes the set if there is a previous webhook
bot_obj.bot.remove_webhook()

time.sleep(1.1)

# Set webhook
WEBHOOK_URL_BASE = 'https://{}:{}'.format(telegram_config.WEBHOOK_HOST, telegram_config.WEBHOOK_PORT)
WEBHOOK_URL_PATH = '/{}'.format(telegram_config.BOT_TOKEN)

bot_obj.bot.set_webhook(url=WEBHOOK_URL_BASE + WEBHOOK_URL_PATH,
                        certificate=open(telegram_config.WEBHOOK_SSL_CERT, 'r'))

app.run(host=telegram_config.WEBHOOK_LISTEN,
        port=telegram_config.WEBHOOK_PORT,
        ssl_context=(telegram_config.WEBHOOK_SSL_CERT, telegram_config.WEBHOOK_SSL_PRIV),
        debug=True)
